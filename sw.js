importScripts("https://storage.googleapis.com/workbox-cdn/releases/3.1.0/workbox-sw.js");
var cacheStorageKey = 'cache-pwa'
var cacheList=[
  '/',
  'index.html',
  'main.css',
  'baseIcon.png'
]

// sw.js首次被注册时候触发
self.addEventListener('install', async event => {
    const cache = await caches.open(cacheStorageKey);
    cache.addAll(staticAssets);
})

// sw监听到fetch事件时候触发
self.addEventListener('fetch', event => {
    const req = event.request;
    const url = new URL(req.url);
    
    // 当本地开发时候可以这么配置
    if (url.origin === location.origin) {
        event.respondWith(cacheFirst(req));
    } else if ((req.url.indexOf('http') !== -1)) {
        // chrome的https协议限制，接口必须满足https
        event.respondWith(networkFirst(req));
    }
});


// 使用浏览器缓存
async function cacheFirst(req) {
    const cachedResponse = await caches.match(req);
    return cachedResponse || fetch(req);
}

// 网络优先
async function networkFirst(req) {
    // 将请求到的数据缓存在id为news-dynamic中
    const cache = await caches.open(cacheStorageKey);

    try {
        const res = await fetch(req); // 获取数据
        cache.put(req, res.clone()); // 更新缓存
        return res;
    } catch (error) {
        return await cache.match(req); // 报错则使用缓存
    }
}
